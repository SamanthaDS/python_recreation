#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Samantha
#
# Created:     04/06/2014
# Copyright:   (c) Samantha 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy as np
import traceback, os, time, timeit

output_file_name = "E:/Recreation/Manuscript/Results/2006NLCD/Sum_of_outputs.txt"
of = open(output_file_name, 'w')
of.write("Sum of days demanded in output arrays \n")

outputs = ["Rural_BigGame_Water_distr_days.txt",
    "Rural_BirdW_Water_distr_days.txt", "Rural_Fish_Water_distr_days.txt",
    "Rural_MBHunt_Water_distr_days.txt", "Urban_BigGame_Water_distr_days.txt",
    "Urban_BirdW_Water_distr_days.txt", "Urban_Fish_Water_distr_days.txt",
    "Urban_MBHunt_Water_distr_days.txt"]
path = "E:/Recreation/Manuscript/Outputs/480m_runs/"

for i in range(8):
    DDD = np.loadtxt(
        path + outputs[i], # File path to output array
        dtype = np.float64,
        delimiter = " "
    )
    total_days = sum(sum(DDD))
    of.write(" Sum of days in output file: " + outputs[i] + " equals " + str(total_days) + '\n')

of.close()