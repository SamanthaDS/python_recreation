#-------------------------------------------------------------------------------
# Name:        Regions Market Segments Script 3
# Purpose:  To clip the Hispanic and Non White Urban Market Segment raster to
# the 10 regions of interest
# Author:      ssifleet
# Created:     15/01/2014
# Copyright:   (c) ssifleet 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
# Import system modules
import arcpy, traceback, os, time
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "E:/Recreation/MarketSegments.gdb"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Create report file
tmpName = time.asctime()
tmpName =  tmpName.replace(':', '-')
reportfileName = "E:/Recreation/Regions_MarketSegments_3_" + tmpName  + ".txt"
print reportfileName
reportFile = open(reportfileName, 'w')
startTime = time.asctime()

try:
# Sam's Code Starts Here!
    # Step 1 - Create Hispanic and Non White Urban Raster for Appalachia
    # Variables
    InRaster = "E:/Recreation/MarketSegments.gdb/HispanicNW_urban" # DO NOT EVER COMMENT OUT THIS LINE!
    ClipPolygon = "E:/Recreation/Regions.gdb/Appalachia"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_Appl"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 2 - Create Hispanic and Non White Urban Raster for Corn Belt
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/CornBelt"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_CornB"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 3 - Create Hispanic and Non White Urban Raster for Delta
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/Delta"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_Delta"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 4 - Create Hispanic and Non White Urban Raster for Lake States
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/LakeStates"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_Lake"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 5 - Create Hispanic and Non White Urban Raster for Mountain
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/Mountain"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_Mount"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 6 - Create Hispanic and Non White Urban Raster for North East
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/NorthEast"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_NorthE"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 7 - Create Hispanic and Non White Urban Raster for Northern Plains
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/NorthernPlains"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_NPlains"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 8 - Create Hispanic and Non White Urban Raster for Pacific
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/Pacific"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_Pacific"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 9 - Create Hispanic and Non WhiteUrban Raster for South East
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/SouthEast"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_SouthE"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
    # Step 10 - Create Hispanic and Non White Urban Raster for Southern Plains
    # Variables
    ClipPolygon = "E:/Recreation/Regions.gdb/SouthernPlains"
    OutRaster = "E:/Recreation/HispanicNW_Urban.gdb/HisNW_U_SPlains"
    # Execute Clip
    arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
# Sam's code ends here

    reportFile.write( "\nCompleted processing file successfully." + '\n')
    reportFile.write( "\n----------------------Potential Issues Found-----------------------------\n")
    # Write issues found out to file
    reportFile.write( "\nStarted at " + startTime + '\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')

    # close the report file
    reportFile.close()

    print "Things went fine - All Done."


except:

    print "Something went wrong."
    print "Traceback Message below:"
    print traceback.format_exc()
    print "ArcMap Messages below:"
    print arcpy.GetMessages(2)

    # write in report that something went wrong
    reportFile.write( "Something went wrong." + '\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')

    # close the report file it still open
    if reportFile:
        reportFile.close()

    # Rename log file bases on outcome
    os.rename(reportfileName, reportfileName[:-4] + "_Fail.txt")