# README #

This repo houses the code for the spatial models presented in the paper [Benefit transfer with limited data: An application to recreational fishing losses from surface mining](https://www.sciencedirect.com/science/article/pii/S0921800915003857)

These models were used to generate the following data sets for the US EPA EnviroAtlas:

1. [Recreation demand for bird watching](https://catalog.data.gov/dataset/enviroatlas-bird-watching-recreation-demand-by-12-digit-huc-in-the-conterminous-united-states)
2. [Recreation demand for big game hunting](https://catalog.data.gov/dataset/enviroatlas-big-game-hunting-recreation-demand-by-12-digit-huc-in-the-conterminous-united-stat)
3. [Recreation demand for migratory bird hunting](https://catalog.data.gov/dataset/enviroatlas-migratory-bird-hunting-recreation-demand-by-12-digit-huc-in-the-conterminous-unite)

This is the repository for the distribution of days demanded over a landscape using a 2 dimensional probability density function of travel distance.

Final model script is [Travel_Distance_Water_480.py'](https://bitbucket.org/SamanthaDS/python_recreation/src/master/Travel_Distance_Water_480.py) all others are prep scripts for different applications.