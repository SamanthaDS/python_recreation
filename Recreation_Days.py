#-------------------------------------------------------------------------------
# Name:        Rcreation Days Calculation
# Purpose: To sum the Urban/Rural participants then to apply the urban/rural
# average number of days demanded by recreation type and region.
#
# Author:      Samantha Sifleet
#
# Created:     03/02/2014
# Copyright:   (c) ssifleet 2014
#-------------------------------------------------------------------------------

# Import system modules
import arcpy, traceback, os, time
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "H:/Recreation/Working.gdb"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Create report file
tmpName = time.asctime()
tmpName =  tmpName.replace(':', '-')
reportfileName = "E:/Recreation/Recreation_Days" + tmpName  + ".txt"
reportFile = open(reportfileName, 'w')
startTime = time.asctime()
print "Run started at " + startTime

# Set variables
Regions = ["Appl", "CornB", "Delta", "Lake", "Mount", "NorthE", "NPlains", "Pacific", "SouthE", "SPlains"]
Recreation = ["H:/Recreation/BigGame_2011.gdb", "H:/Recreation/BirdW_2011.gdb","H:/Recreation/FFish_2011.gdb", "H:/Recreation/MBHunt_2011.gdb"]
BigGame_R = [22.52, 18.49, 23.92, 17.68, 14.61, 20.45, 11.78, 12.26, 24.11, 16.95]
BigGame_U = [19.60, 15.98, 16.88, 13.18, 12.06, 19.06, 10.90, 13.56, 19.59, 15.90]
BirdWatching_R = [12.84, 16.07, 32.25, 17.32, 16.93, 20.95, 17.40, 10.69, 9.63, 11.07]
BirdWatching_U = [12.89, 10.60, 11.00, 14.00, 14.91, 22.20, 13.91, 13.58, 12.41, 11.52]
FreshFish_R = [21.54, 21.09, 24.66, 22.26, 16.68, 21.16, 19.99, 18.55, 23.14, 15.94]
FreshFish_U = [17.03, 17.01, 18.68, 20.79, 14.29, 15.14, 13.82, 16.62, 17.09, 21.85]
MigratoryBird_R = [21.54, 11.07, 12.36, 7.06, 10.25, 9.65, 7.39, 14.53, 7.06, 4.13]
MigratoryBird_U = [17.03, 6.96, 8.29, 9.95, 12.31, 11.03, 9.09, 9.47, 3.13, 9.30] # Used National rate for lake States due to low sample size
Rural_days = [BigGame_R, BirdWatching_R, FreshFish_R, MigratoryBird_R]
Urban_days = [BigGame_U, BirdWatching_U, FreshFish_U, MigratoryBird_U]

# Main Code Block
try:
    for num in xrange(4):
        print "Beginning "+Recreation[num]+" processing at " + time.asctime()
        for i in xrange(10):
            # Rural Calcs - first add the two rural groups of participants together
            a = Recreation[num] + "/R_nwhis_"+ Regions[i]
            b = Recreation[num] + "/R_nhwhite_" + Regions[i]
            Outras = Raster(a)+Raster(b)
            Outras.save(Recreation[num] + "/Rural_"+Regions[i])
            # Now multiply the combined Rural participants by the average rural # of days
            R_days = Raster(Recreation[num] + "/Rural_"+Regions[i])*Rural_days[num][i]
            R_days.save(Recreation[num] + "/Days_Rural_"+Regions[i])
            # Urban Calcs - first add the two urban groups of participants together
            a = Recreation[num] + "/U_nwhis_"+ Regions[i]
            b = Recreation[num] + "/U_nhwhite_" + Regions[i]
            Outras = Raster(a)+Raster(b)
            Outras.save(Recreation[num] + "/Urban_"+Regions[i])
            # Now multiply the combined Urban participants by the average urban # of days
            U_days = Raster(Recreation[num] + "/Urban_"+Regions[i])*Urban_days[num][i]
            U_days.save(Recreation[num] + "/Days_Urban_"+Regions[i])
            print "Completed processing for " + Regions[i] +"at"+ time.asctime()

    reportFile.write( "\nCompleted processing file successfully." + '\n')
    # Write issues found out to file
    reportFile.write( "\nStarted at " + startTime + '\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')
    # close the report file
    reportFile.close()
    print "Things went fine - All Done."

except:
    print "Something went wrong."
    print "Traceback Message below:"
    print traceback.format_exc()
    print "ArcMap Messages below:"
    print arcpy.GetMessages(2)
    # write in report that something went wrong
    reportFile.write( "Something went wrong." + '\n')
    reportFile.write( "Traceback Message below:"+ '\n')
    reportFile.write(traceback.format_exc() +'\n')
    reportFile.write( "ArcMap Messages below:"+'\n')
    reportFile.write( arcpy.GetMessages(2) +'\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')
    # close the report file it still open
    if reportFile:
        reportFile.close()
    # Rename log file bases on outcome
    os.rename(reportfileName, reportfileName[:-4] + "_Fail.txt")
