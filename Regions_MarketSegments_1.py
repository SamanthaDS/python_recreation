#-------------------------------------------------------------------------------
# Name:        Regions Market Segments Script
# Purpose:  To clip the four market segment rasters to the 10 regions of interest
# Author:      ssifleet
# Created:     15/01/2014
# Copyright:   (c) ssifleet 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
# Import system modules
import arcpy, traceback, os, time
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "H:/Recreation/Working.gdb/"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Create report file
tmpName = time.asctime()
tmpName =  tmpName.replace(':', '-')
reportfileName = "H:/Recreation/Reports_From_Python_Scripts/Regions_MarketSegments_" + tmpName  + ".txt"
print reportfileName
reportFile = open(reportfileName, 'w')
startTime = time.asctime()

# Define globals
base_path = "H:/Recreation/"
market_path = "H:/Recreation/Working.gdb/"
markets = ["Rural_hispanic", "Rural_white", "Urban_hispanic", "Urban_white"]
market_abbr = ["R_nwhis", "R_nhwhite", "U_nwhis", "U_nhwhite"]
reg_path = "H:/Recreation/Regions.gdb/"
regions = ["Appalachia", "CornBelt", "Delta", "LakeStates", "Mountain", "NorthEast", "NorthernPlains", "Pacific", "SouthEast", "SouthernPlains"]
regions_abbr = ["Appl","CornB", "Delta", "Lake", "Mount", "NorthE", "NPlains", "Pacific", "SouthE", "SPlains"]

try:
    for i in range(4):
        InRaster = market_path + markets[i]
        print "Processing " + markets[i] + " market"
        for a in range(10):
            ClipPolygon = reg_path + regions[a]
            OutRaster = base_path + market_abbr[i] + ".gdb/" + market_abbr[i] + "_" + regions_abbr[a]
            # Execute Clip
            arcpy.Clip_management(InRaster, "#", OutRaster, ClipPolygon, "#", "ClippingGeometry")
            print "Completed " + regions[a] + " Clip"

    reportFile.write( "\nCompleted processing file successfully." + '\n')
    reportFile.write( "\n----------------------Potential Issues Found-----------------------------\n")
    # Write issues found out to file
    reportFile.write( "\nStarted at " + startTime + '\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')

    # close the report file
    reportFile.close()

    print "Things went fine - All Done."


except:

    print "Something went wrong."
    print "Traceback Message below:"
    print traceback.format_exc()
    print "ArcMap Messages below:"
    print arcpy.GetMessages(2)

    # write in report that something went wrong
    reportFile.write( "Something went wrong." + '\n')
    reportFile.write( "Traceback Message below:"+ '\n')
    reportFile.write(traceback.format_exc() +'\n')
    reportFile.write( "ArcMap Messages below:"+'\n')
    reportFile.write( arcpy.GetMessages(2) +'\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')

    # close the report file
    if reportFile:
        reportFile.close()

    # Rename log file based on outcome
    os.rename(reportfileName, reportfileName[:-4] + "_Fail.txt")