#-------------------------------------------------------------------------------
# Name:        Recreation Participation Calculations
# Purpose: To apply the national participation rates to the Non-White and
# Hispanic market segments and the regional participation rates
# to the Non-Hispanic White market segments four four types of outdoor recreation.
#
# Author:      Samantha Sifleet
#
# Created:     31/01/2014
# Copyright:   (c) ssifleet 2014
#-------------------------------------------------------------------------------

# Import system modules
import arcpy, traceback, os, time
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "H:/Recreation/Working.gdb"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Create report file
tmpName = time.asctime()
tmpName =  tmpName.replace(':', '-')
reportfileName = "H:/Recreation/Reports_From_Python_Scripts/Recreation_Participation_" + tmpName  + ".txt"
reportFile = open(reportfileName, 'w')
startTime = time.asctime()
print "Run started at " + startTime

# Set Global variables
Regions = ["Appl", "CornB", "Delta", "Lake", "Mount", "NorthE", "NPlains", "Pacific", "SouthE", "SPlains"]
BG_NHW_R = [0.099, 0.120, 0.210, 0.154, 0.102, 0.113, 0.126, 0.052, 0.117, 0.092]
BG_NHW_U = [0.031, 0.028, 0.057, 0.076, 0.023, 0.024, 0.048, 0.018, 0.025, 0.037]
BW_NHW_R = [0.077, 0.050, 0.067, 0.080, 0.091, 0.073, 0.074, 0.143, 0.084, 0.100]
BW_NHW_U = [0.090, 0.075, 0.051, 0.033, 0.110, 0.083, 0.069, 0.098, 0.099, 0.056]
FF_NHW_R = [0.164, 0.180, 0.267, 0.283, 0.183, 0.121, 0.223, 0.125, 0.178, 0.156]
FF_NHW_U = [0.098, 0.119, 0.147, 0.145, 0.125, 0.065, 0.141, 0.072, 0.074, 0.122]
MB_NHW_R = [0.018, 0.012, 0.058, 0.032, 0.026, 0.010, 0.046, 0.019, 0.018, 0.055]
MB_NHW_U = [0.008, 0.014, 0.015, 0.009, 0.007, 0.002, 0.018, 0.008, 0.007, 0.015]

# Main Code Block
try:
    Raster_base = "H:/Recreation/R_nwhis.gdb/R_nwhis_" # Apply National Participation Rates
    print "Beginning Rural Hispanic & Non White market segment processing at " + time.asctime()
    for i in Regions:
            raster = Raster_base+i
            Outras = Raster(raster)*0.034
            Outras.save("H:/Recreation/BigGame_2011.gdb/R_nwhis_"+i)
            Outras = Raster(raster)*0.039
            Outras.save("H:/Recreation/BirdW_2011.gdb/R_nwhis_"+i)
            Outras = Raster(raster)*0.097
            Outras.save("H:/Recreation/FFish_2011.gdb/R_nwhis_"+i)
            Outras = Raster(raster)*0.004
            Outras.save("H:/Recreation/MBHunt_2011.gdb/R_nwhis_"+i)
            print "Completed Hispanic & non White Rural market segment for " + i + " at " + time.asctime()
    Raster_base ="H:/Recreation/U_nwhis.gdb/U_nwhis_" # Apply National participation Rates
    print "Beginning Hispanic & Non White Urban market segment processing at " + time.asctime()
    for i in Regions:
            raster = Raster_base+i
            Outras = Raster(raster)*0.006
            Outras.save("H:/Recreation/BigGame_2011.gdb/U_nwhis_"+i)
            Outras = Raster(raster)*0.020
            Outras.save("H:/Recreation/BirdW_2011.gdb/U_nwhis_"+i)
            Outras = Raster(raster)*0.038
            Outras.save("H:/Recreation/FFish_2011.gdb/U_nwhis_"+i)
            Outras = Raster(raster)*0.001
            Outras.save("H:/Recreation/MBHunt_2011.gdb/U_nwhis_"+i)
            print "Completed Hispanic & non White Urban market segment for " + i + " at " + time.asctime()
    Raster_base = "H:/Recreation/R_nhwhite.gdb/R_nhwhite_" # Apply Regional Participation Rates
    print "Beginning Non-Hispanic White Rural market segment processing at " + time.asctime()
    for i in xrange(10):
            raster = Raster_base+Regions[i]
            Outras = Raster(raster)*BG_NHW_R[i]
            Outras.save("H:/Recreation/BigGame_2011.gdb/R_nhwhite_"+Regions[i])
            Outras = Raster(raster)*BW_NHW_R[i]
            Outras.save("H:/Recreation/BirdW_2011.gdb/R_nhwhite_"+Regions[i])
            Outras = Raster(raster)*FF_NHW_R[i]
            Outras.save("H:/Recreation/FFish_2011.gdb/R_nhwhite_"+Regions[i])
            Outras = Raster(raster)*MB_NHW_R[i]
            Outras.save("H:/Recreation/MBHunt_2011.gdb/R_nhwhite_"+Regions[i])
            print "Completed Non-Hispanic White Rural market segment for " + Regions[i] + " at " + time.asctime()
    Raster_base = "H:/Recreation/U_nhwhite.gdb/U_nhwhite_" # Apply Regional Participation Rates
    print "Beginning Non-Hispanic White Urban market segment processing at " + time.asctime()
    for i in xrange(10):
            raster = Raster_base+Regions[i]
            Outras = Raster(raster)*BG_NHW_R[i]
            Outras.save("H:/Recreation/BigGame_2011.gdb/U_nhwhite_"+Regions[i])
            Outras = Raster(raster)*BW_NHW_R[i]
            Outras.save("H:/Recreation/BirdW_2011.gdb/U_nhwhite_"+Regions[i])
            Outras = Raster(raster)*FF_NHW_R[i]
            Outras.save("H:/Recreation/FFish_2011.gdb/U_nhwhite_"+Regions[i])
            Outras = Raster(raster)*MB_NHW_R[i]
            Outras.save("H:/Recreation/MBHunt_2011.gdb/U_nhwhite_"+Regions[i])
            print "Completed Non-Hispanic White Urban market segment for " + Regions[i] + " at " + time.asctime()
    reportFile.write( "\nCompleted processing file successfully." + '\n')
    # Write issues found out to file
    reportFile.write( "\nStarted at " + startTime + '\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')
    # close the report file
    reportFile.close()
    print "Things went fine - All Done."

except:
    print "Something went wrong."
    print "Traceback Message below:"
    print traceback.format_exc()
    print "ArcMap Messages below:"
    print arcpy.GetMessages(2)
    # write in report that something went wrong
    reportFile.write( "Something went wrong." + '\n')
    reportFile.write( "Traceback Message below:"+ '\n')
    reportFile.write(traceback.format_exc() +'\n')
    reportFile.write( "ArcMap Messages below:"+'\n')
    reportFile.write( arcpy.GetMessages(2) +'\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')
    # close the report file it still open
    if reportFile:
        reportFile.close()
    # Rename log file bases on outcome
    os.rename(reportfileName, reportfileName[:-4] + "_Fail.txt")