#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Samantha
#
# Created:     04/06/2014
# Copyright:   (c) Samantha 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy as np
import traceback, os, time, timeit

output_file_name = "E:/Recreation/Manuscript/Results/2006NLCD/Sum_of_inputs.txt"
of = open(output_file_name, 'w')
of.write("Sum of days demanded in input arrays \n")

inputs = ["Rural_BigGame_Demand_Array.txt",
    "Rural_BirdW_Demand_Array.txt", "Rural_Fish_Demand_Array.txt",
    "Rural_MBHunt_Demand_Array.txt", "Urban_BigGame_Demand_Array.txt",
    "Urban_BirdW_Demand_Array.txt", "Urban_Fish_Demand_Array.txt",
    "Urban_MBHunt_Demand_Array.txt"]
path = "E:/Recreation/Manuscript/Numpy_rasters/480m_array/"
for i in range(8):
    DDD = np.loadtxt(
        path + inputs[i], # File path to input array
        dtype = np.float64,
        delimiter = " "
    )
    DDD[DDD<0] = 0 #convert all negatives (water pixels) to zeros
    total_days = sum(sum(DDD))
    of.write(" Sum of days in input file: " + inputs[i] + " equals " + str(total_days) + '\n')

of.close()