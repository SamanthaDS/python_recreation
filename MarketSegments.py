#-------------------------------------------------------------------------------
# Name:        Market Segments
# Purpose: To generate the three remaining market segments for recreational
# demand analsysis for the EnviroAtlas
# Author:      ssifleet
# Created:     15/01/2014
# Copyright:   (c) ssifleet 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
# Import system modules
import arcpy, traceback, os, time
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Create report file
tmpName = time.asctime()
tmpName =  tmpName.replace(':', '-')
reportfileName = r"C:\Users\Samantha\Documents\MarketSegments_" + tmpName  + ".txt"
print reportfileName
reportFile = open(reportfileName, 'w')
startTime = time.asctime()

try:
    # Sam's Code Starts Here!
##    # Step 1 - Create Non-Hispanic White Amount Raster
##    dasy_2011 = "F:/Recreation/US_DASY_NLCD2011.tif"
##    NHW_perc_by_block = "F:/Recreation/US_NHWHITE_percentByBlock.tif"
##    env.workspace = "F:/Recreation/Working.gdb"
##    Outras = Raster(dasy_2011)*Raster(NHW_perc_by_block)
##    Outras.save("NHW_amount")
##    # Step 2 - Create Non-white and Hispanic Amount Raster
##    NHW_amt = "F:/Recreation/Working.gdb/NHW_amount"
##    Outras = Raster(dasy_2011)-Raster(NHW_amt)
##    Outras.save("NWHis_amount")
    # Step 3 - Create Non-Hispanic White Urban Market Segment
    urban = "F:/Recreation/MarketSegments_2011.gdb/urban"
    NHW_amt = r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb\Times"
    outExtractByMask = ExtractByMask(NHW_amt, urban)
    outExtractByMask.save(r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb\NHW_urban")
    # Step 4 - Create Non-Hispanic White Rural Market Segment
    rural = "F:/Recreation/MarketSegments_2011.gdb/rural"
    outExtractByMask = ExtractByMask(NHW_amt, rural)
    outExtractByMask.save(r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb\NHW_rural")
    # Step 5 - Create Non White + Hispanic Urban Market Segment
    NWHis_amt =r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb\Hispanic"
    outExtractByMask = ExtractByMask(NWHis_amt, urban)
    outExtractByMask.save(r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb\NWHis_urban")
    # Step 6 - Create Non White + Hispanic Rural Market Segment
    outExtractByMask = ExtractByMask(NWHis_amt, rural)
    outExtractByMask.save(r"C:\Users\Samantha\Documents\Dasy_prep_2011.gdb\NWHis_rural")
# Sam's code ends here

    reportFile.write( "\nCompleted processing file successfully." + '\n')
    reportFile.write( "\nStarted at " + startTime + '\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')
    # close the report file
    reportFile.close()
    print "Things went fine - All Done."

except:

    print "Something went wrong."
    print "Traceback Message below:"
    print traceback.format_exc()
    print "ArcMap Messages below:"
    print arcpy.GetMessages(2)

    # write in report that something went wrong
    reportFile.write( "Something went wrong." + '\n')
    reportFile.write( "Traceback Message below:"+ '\n')
    reportFile.write(traceback.format_exc() +'\n')
    reportFile.write( "ArcMap Messages below:"+'\n')
    reportFile.write( arcpy.GetMessages(2) +'\n')
    reportFile.write( "Ended at " + time.asctime() + '\n')

    # close the report file it still open
    if reportFile:
        reportFile.close()

    # Rename log file bases on outcome
    os.rename(reportfileName, reportfileName[:-4] + "_Fail.txt")