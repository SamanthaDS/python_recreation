#-------------------------------------------------------------------------------
# Name: Recreation Travel Distance Demand Module
# Purpose: This module includes functions to disperse the recreation days demanded by origin (pixel)
# using a two-dimensional probability density function of willingness to travel.
#
# Author:      ssifleet
#
# Created:     04/04/2014
# Copyright:   (c) ssifleet 2014
#-------------------------------------------------------------------------------

import traceback, os, time, timeit
import numpy as np
from copy import copy, deepcopy

## Could add input paths as variables here and remove from the def init fx
# Define global variables (These must be set for EVERY run)
path = "C:\data" # Path to folder where all outputs and reports will be stored
service = "test1" # Recreation Service (must be one of (BigGame, MBHunt, FFish, or BirdW))
UR = "test1123" # urban/rural status

def init_data(): #Please create a similar function to aid in naming the outputs uniformly
    """This function initializes the input data. The full file paths need to be
    entered here for each run"""
    demand_array = np.loadtxt(
        "C:/Users/Samantha/Documents/Recreation_Manuscript/Numpy_rasters/480m_array/Rural_Fish_Demand_Array.txt", # File path to demand array
        dtype = np.float64,
        delimiter = " "
    )
    pdf_array = np.loadtxt(
        "C:/Users/Samantha/Documents/Recreation_Manuscript/Two_d_pdfs/Fish_Rural_2d_480_pdf.txt", # File path to 2-d pdf
        dtype = np.float64,
        delimiter = " "
    )
    water = np.loadtxt(
        "C:/Users/Samantha/Documents/Recreation_Manuscript/Numpy_rasters/480m_array/Water_Array2.txt", # File path to water/boundary array
        dtype = np.float64,
        delimiter = " "
    )
    zeros = np.zeros_like(demand_array, dtype = np.float)
    big_zero_array = np.zeros((2882, 1568)) # make this cleaner
    return demand_array, pdf_array, water, zeros, big_zero_array

def full_run():
    """This is the master function that first checks for water (or other areas outside the boundaries)
    then selects the appropriate functions to run.  This is the only function you should need to run.
    All other functions within this module will be called through this function."""
    start_time = time.asctime()
    print "Run started at:", start_time
    report_file_name = path + service + "_" + UR + "_" + "QA_Report.txt"
    if not water_check():
        water_note = "Water Detected"
        print water_note
        run_water()
    else:
        water_note = "No Water Detected"
        print water_note
        run_fast()

def log_completetion(value_count, row, col):
    """This function generates the run completion QA Report"""
    with open(report_file_name, 'a') as fh:
        fh.write( "\nCompleted processing file successfully." + '\n')
        fh.write( "\nStarted at " + start_time + '\n')
        fh.write( water_note + '\n')
        fh.write( "Ended at " + time.asctime() + '\n')
        fh.write( "Value Count = " + str(value_count) + '\n')
        fh.write( "End Row = " + str(row) + '\n')
        fh.write( "End Column = " + str(col) + '\n')

    print value_count
    print "Row = ", row
    print "Column = ", col
    print "Run ended at " + time.asctime()

def notifiy_error(error_message):
    """This function generates the run failure QA report"""
    print "Something went wrong."
    print "Traceback Message below:"
    print error_message
    # write in report that something went wrong
    with open(report_file_name, 'a') as fh:
        reportFile.write( "Something went wrong." + '\n')
        reportFile.write( "Traceback Message below:"+ '\n')
        reportFile.write(traceback.format_exc() +'\n')
    # Rename log file bases on outcome
    os.rename(report_file_name, report_file_name[:-4] + "_Fail.txt")


def get_cols(col):
    """This function generates the columns full of zeros used to spatially manuever
    the 2-d pdf around the demand array"""
    return big_zero_array[0:2882, 0:(col-250)], big_zero_array[0:2882, 0:(1317 - col)]

def get_rows(row):
    """This function generates the rows full of zeros used to spatially manuever
    the 2-d pdf around the demand array"""
    return big_zero_array[0:(row-250), 0:501 ], big_zero_array[0:(2631 - row), 0:501]

def water_check():
    """This function check to see if there is water (or other areas outside the boundaries)
    present in the demand array"""
    demand_array, pdf_array, water, zeros, big_zero_array = init_data()
    negatives = demand_array[demand_array<0]
    if negatives.size > 0 :
        return False
    else:
        return True

def run_fast(save_every = 400000, step_size = 1):
    """This funcion runs the 2-d dispersion of the days demanded by origin only when
    water or other areas outside the boundaries) are not contained within the demand array"""
    demand_array, pdf_array, water, zeros, big_zero_array = init_data()
    row = 250
    col = 250
    temp_it = 0 # iterator
    value_count = 0 # QA variable
    while temp_it < 2543976: # total number of cells in demand array
        if demand_array[row, col] > 0:
            value_count += 1
            val = demand_array[row, col] # get value
            val_pdf_array = pdf_array*val # multiply pdf by value
            top_rows, bottom_rows = get_rows(row) # shift pdf
            row_stack = np.vstack((top_rows, val_pdf_array, bottom_rows)) # shift pdf
            left_cols, right_cols = get_cols(col) # shift pdf
            col_stack = np.hstack((left_cols, row_stack, right_cols)) # shift pdf
            # Step 3 add to the zeros
            zeros = zeros + col_stack
        col += step_size
        if col > 1317:
            col = 250
            row += step_size
        if not temp_it % save_every: # save intermediates
            np.savetxt(
                path + UR + "_" + service + "_%d.txt" % temp_it,
                zeros,
                fmt = '%.10f',
                delimiter = " "
            )
            print "Iteration " + str(temp_it) + " completed at " + time.asctime()
            print "val_count equals " + str(value_count)
        temp_it+= 1

    np.savetxt(
        path + UR +"_" + service + "_" + "distr_days.txt",
        zeros,
        fmt='%.10f',
        delimiter = " "
    )
    log_completetion(value_count, row, col)

def run_water(save_every = 400000, step_size = 1):
    """This funcion runs the 2-d dispersion of the days demanded by origin only when
    water or other areas outside the boundaries) are present within the demand array"""
    demand_array, pdf_array, water, zeros, big_zero_array = init_data()
    row = 250
    col = 250
    temp_it = 0 # iterator
    value_count = 0 #Q A variable
    while temp_it < 2543976: # total number of cells in demand array
        if demand_array[row, col] > 0:
            value_count += 1
            val = demand_array[row, col] # get value
            top_rows, bottom_rows = get_rows(row)
            row_stack = np.vstack((top_rows, pdf_array, bottom_rows)) # shift pdf
            left_cols, right_cols = get_cols(col) # shift pdf
            col_stack = np.hstack((left_cols, row_stack, right_cols)) # shift pdf
            water_pdf = water + col_stack # look for water (-1) overlapping with shifted pdf
            water_pdf[water_pdf<0] = 0 # convert negatives to zeros
            pos_tot = sum(sum(water_pdf)) # get the sum of the pdf (should be one if no water - but we know it's not)
            pos_tot_round = round(pos_tot, 10) # round the sum just to make sure
            if pos_tot_round == 1: # no water in pdf
                col_stack = col_stack*val # multiply shifted pdf by value
                zeros = zeros + col_stack
            elif pos_tot < 1: # water in pdf
                water_pdf_rescale = water_pdf/pos_tot # rescale pdf values to sum to one
                water_pdf_rescale=water_pdf_rescale*val # multiply shifted and resclaed pdf by value
                zeros = zeros + water_pdf_rescale
            else:
                print "Error! This should never happen!"
                break
        col += step_size
        if col > 1317:
            col = 250
            row += step_size
        if not temp_it % save_every:
            np.savetxt(
                path + UR + "_" + service + "_water_%d.txt" % temp_it,
                zeros,
                fmt = '%.10f',
                delimiter = " "
            )
            print "Iteration " + str(temp_it) + " completed at " + time.asctime()
            print "val_count equals " + str(value_count)
        temp_it+= 1

    np.savetxt(
        path + UR +"_" + service + "_" + "distr_days.txt",
        zeros,
        fmt='%.10f',
        delimiter = " "
    )
    log_completetion(value_count, row, col)